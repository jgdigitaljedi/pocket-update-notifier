import chalk from 'chalk';
import { writeFileSync } from 'node:fs';
import { dirname, join } from 'node:path';
import { fileURLToPath } from 'url';
import myCores from './data/myCores.json' assert { type: 'json' };
import config from './config.json' assert { type: 'json' };
import { downloadCore } from './common/downloadCore.mjs';
import { deleteNewCoresDir, deleteZipFiles } from './common/deleteNewCoresDir.mjs';
import { unzipNewCores } from './common/unzipCores.mjs';
import { getPocketFirmwareVersion } from './common/getPocketFirmwareVersion.mjs';
import { generateTable } from './extraScripts/generateTableForReadme/generateTable.mjs';
import { updateMasterRepoList } from './common/updateMasterRepoList.mjs';
import { statusEmo } from './common/helpers.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

function compareVersions(current, result) {
  return current.myVersion !== result.release;
}

function writeNewUpdateFile(comparedData) {
  const updatedIds = comparedData.map((c) => c.id);
  const newData = myCores.map((item) => {
    const comparedIndex = updatedIds.indexOf(item.id);
    if (comparedIndex > -1) {
      return {
        id: comparedData[comparedIndex].id,
        myVersion: comparedData[comparedIndex].release
      };
    }
    return item;
  });
  writeFileSync(join(__dirname, './data/updatedMyCores.json'), JSON.stringify(newData, null, 2));
}

(async function () {
  // delete cores currently in newCores directory
  if (config.deleteNewCoresBeforeRun) {
    await deleteNewCoresDir();
  }

  const updatedMasterList = await updateMasterRepoList();

  // get latest version info for your currently used cores
  const myCoresLen = myCores.length;
  const ids = updatedMasterList.map((c) => c.id);
  const comparedData = [];
  for (let i = 0; i < myCoresLen; i++) {
    const masterIndex = ids.indexOf(myCores[i].id);
    const currentApiCore = updatedMasterList[masterIndex];
    try {
      const compared = compareVersions(myCores[i], currentApiCore);
      if (compared) {
        comparedData.push({
          updateInfo: `current: ${myCores[i]['myVersion']} -> new: ${currentApiCore.release}`,
          updateType: compared,
          ...currentApiCore
        });
      }
      console.log(
        chalk.cyan(
          `${statusEmo.check} Successfully fetched new release info for ${currentApiCore.id} core!`
        )
      );
    } catch (error) {
      console.log(
        chalk.red.bold(
          `${statusEmo.fire} Error fetching release info for ${myCores[i].id} core.`,
          error
        )
      );
    }
  }

  // write info for any cores that have updates to coreUpdates.json
  writeFileSync(join(__dirname, './data/coreUpdates.json'), JSON.stringify(comparedData, null, 2));

  // write new myCores.json config wiwth latest version to updatedMyCores.json
  if (config.writeNewCoresFile) {
    writeNewUpdateFile(comparedData);
  }

  // compare latest versions with your current versions
  if (comparedData?.length) {
    console.log(chalk.green.bold(`\n\n${statusEmo.info} Available updates:`));
    const dataLen = comparedData.length;
    for (let i = 0; i < dataLen; i++) {
      const currentCore = comparedData[i];
      console.log(
        chalk.yellow.bold(
          `${statusEmo.game} Update for ${currentCore.id} core (${currentCore.updateInfo}):`
        )
      );
      console.log(chalk.cyan(`${currentCore.download}`));
      if (config.downloadUpdates && (config.personalAccessToken || process.env.GITHUB_KEY)) {
        try {
          // download any cores that have updates
          await downloadCore(
            currentCore,
            `${statusEmo.fire} Error downloading zip for ${currentCore.platform}. Use download link in console or util/coreUpdates.json`
          );
          console.log(chalk.magenta.bold(`${currentCore.platform} core downloaded!\n\n`));
        } catch (error) {
          console.log(
            chalk.bgRed.bold(
              `${statusEmo.ex} Couldn't download ${currentCore.platform} core. Use manual download link from console or currentReleases.json`,
              error
            )
          );
        }
      } else {
        // no github token so show repo link
        console.log(
          chalk.cyan(`${statusEmo.down} You can download the update at ${currentCore.download}`)
        );
      }
    }
    try {
      // unzip downloaded cores
      if (config.downloadUpdates && config.extractUpdates) {
        await unzipNewCores();
      }

      // attempt to delete downloaded zips after extraction
      if (config.downloadUpdates && config.extractUpdates && config.deleteZips) {
        await deleteZipFiles(); // this is flaky, not a big deal, but will look into it
      }
    } catch (error) {
      console.log(chalk.red.bold(error));
    }
  } else {
    console.log(chalk.green(`\n\n${statusEmo.check} Your cores are currently up to date.`));
  }

  // check for firmware updates
  if (config.checkFwUpdates) {
    await getPocketFirmwareVersion();
  }

  // env variable so I can leave this setting blank in config as default and still run on my machine
  if (process.env.RUN_UPDATE_ALL_CORES || config.runGetAllCoreInfo) {
    generateTable(updatedMasterList);
  }
})();
