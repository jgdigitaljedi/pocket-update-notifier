import { writeFileSync } from 'node:fs';
import { dirname, join } from 'node:path';
import { fileURLToPath } from 'node:url';
import updatedMyCores from '../../data/updatedMyCores.json' assert { type: 'json' };

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

(function () {
  writeFileSync(
    join(__dirname, '../../data/myCores.json'),
    JSON.stringify(updatedMyCores, null, 2)
  );
})();
