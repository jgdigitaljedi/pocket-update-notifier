import { getPocketFirmwareVersion } from '../../common/getPocketFirmwareVersion.mjs';

(async function () {
  await getPocketFirmwareVersion();
})();
