import { readdirSync, readFileSync, writeFileSync } from 'node:fs';
import { dirname, join } from 'node:path';
import { fileURLToPath } from 'url';
import chalk from 'chalk';
import config from '../../config.json' assert { type: 'json' };
import masterList from '../../data/masterRepoList.json' assert { type: 'json' };
import { statusEmo } from '../../common/helpers.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const mlFormatted = Object.keys(masterList).reduce((acc, key) => {
  acc[masterList[key].shortName] = masterList[key];
  return acc;
}, {});

const coreVerMismatchData = {
  nullobject: 'v',
  Spiritualized: 'v',
  spacemen3: 'v'
};

(function () {
  try {
    const coresDir = join(config.microSdPath, 'Cores');
    const cores = readdirSync(coresDir, 'utf-8');
    const versions = cores.map((core) => {
      const coreJson = readFileSync(join(coresDir, core, 'core.json'), 'utf-8');
      const coreInfo = JSON.parse(coreJson).core.metadata;
      const userCore = {
        version: coreInfo.version,
        url: coreInfo.url,
        shortName: coreInfo.shortname,
        author: coreInfo.author
      };
      return userCore;
    });
    const output = versions.map((core) => {
      const prefix = coreVerMismatchData.hasOwnProperty(core.author)
        ? coreVerMismatchData[core.author]
        : '';
      return {
        id: mlFormatted[core.shortName].id,
        myVersion: `${prefix}${core.version}`
      };
    });
    writeFileSync(join(__dirname, 'coresScraped.json'), JSON.stringify(output, null, 2));
    console.log(
      chalk.green(
        `${statusEmo.check} Success! Your core data was read and your cores files can be found in extraScripts/coreDataFromSdCard/coresScraped.json`
      )
    );
  } catch (err) {
    console.log(
      chalk.red.bold(`${statusEmo.fire} Error reading your cores from your micro SD card: `, err)
    );
  }
})();
