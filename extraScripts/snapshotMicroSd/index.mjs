import { readdirSync, readFileSync, writeFileSync } from 'node:fs';
import { dirname, extname, join } from 'node:path';
import { fileURLToPath } from 'url';
import chalk from 'chalk';
import config from '../../config.json' assert { type: 'json' };
import directoryTree from 'directory-tree';
import flattenDeep from 'lodash.flattendeep';
import { statusEmo } from '../../common/helpers.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const writeIndividual = (filesDirs, dir) => {
  if (filesDirs) {
    const fixed = filesDirs.children.map((c) => {
      if (c.hasOwnProperty('children')) {
        return c.children;
      }
      return c;
    });
    const flat = flattenDeep(fixed)
      .filter((i) => !i.hasOwnProperty('children'))
      .map((i) => i.name)
      .sort();
    return writeFileSync(join(__dirname, `perPlatform/${dir}.json`), JSON.stringify(flat, null, 2));
  }
};

const getAssets = () => {
  const dirs = readdirSync(join(config.microSdPath, 'Assets'), 'utf-8');
  const data = dirs.reduce((acc, dir) => {
    if (dir === 'ng') {
      // because NG roms have many files, just read which dirs are there
      const filesDirs = directoryTree(join(config.microSdPath, 'Assets', dir, 'common'), {
        depth: 1,
        exclude: /.lo/
      });
      acc[dir] = filesDirs;
      writeIndividual(filesDirs, dir);
    } else {
      const filesDirs = directoryTree(join(config.microSdPath, 'Assets', dir, 'common'), {
        attributes: ['extension']
      });
      acc[dir] = filesDirs;
      writeIndividual(filesDirs, dir);
    }

    return acc;
  }, {});
  return data;
};

const getCores = () => {
  const dirs = readdirSync(join(config.microSdPath, 'Cores'), 'utf-8');
  const data = dirs.map((dir) => {
    const coreJson = readFileSync(join(config.microSdPath, 'Cores', dir, 'core.json'), 'utf-8');
    const parsed = JSON.parse(coreJson).core.metadata;
    return {
      shortname: parsed.shortname,
      description: parsed.description,
      author: parsed.author,
      url: parsed.url,
      version: parsed.version,
      date_release: parsed.date_release
    };
  });
  return data;
};

const getFw = () => {
  const fw = {};
  const pocketJson = readFileSync(join(config.microSdPath, 'Analogue_Pocket.json'), 'utf-8');
  fw.info = JSON.parse(pocketJson).firmware.runtime;
  const rootFiles = readdirSync(config.microSdPath);
  const binFiles = rootFiles.filter((file) => extname(file) === '.bin');
  const fwFile = binFiles.filter((file) => file.toLowerCase().indexOf('firmware') > -1);

  if (fwFile?.length) {
    fw.file = fwFile[0];
  }
  return fw;
};

(function () {
  let assets, cores, firmware;
  try {
    assets = getAssets();
    console.log(chalk.green(`${statusEmo.check} Assets successfully read.`));
  } catch (error) {
    console.log(chalk.red(`${statusEmo.fire} Error reading assets`, error));
  }

  try {
    cores = getCores();
    console.log(chalk.green(`${statusEmo.check} Cores successfully read.`));
  } catch (error) {
    console.log(chalk.red(`${statusEmo.fire} Error reading cores`, error));
  }

  try {
    firmware = getFw();
    if (firmware) {
      console.log(chalk.green(`${statusEmo.check} Firmware file successfully read.`));
    } else {
      console.log(chalk.cyan(`${statusEmo.info} No firmware file found on micro SD card.`));
    }
  } catch (error) {
    console.log(chalk.red('Error reading firmware', error));
  }

  try {
    const outName = `snapshot-${new Date().toISOString()}.json`;
    writeFileSync(join(__dirname, outName), JSON.stringify({ firmware, cores, assets }, null, 2));
    console.log(chalk.green.bold(`${statusEmo.check} Snapshot data written to ${outName}`));
  } catch (error) {
    console.log(chalk.red.bold(`${statusEmo.fire} Error writing output`, error));
  }
})();
