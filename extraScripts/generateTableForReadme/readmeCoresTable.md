
<table>
  <thead>
    <tr>
      <th>Core</th>
      <th>Platform</th>
      <th>Latest</th>
      <th>Date Updated</th>
      <th>Developer</th>
    </tr>
  </thead>
  <tbody>
      <tr>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-Robotron">openFPGA-Robotron</a></td>
        <td>Robotron</td>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-Robotron/releases">v0.9.1</a></td>
        <td>2024-03-09</td>
        <td>obsidian-dot-dev</td>
      </tr>
      <tr>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-Defender">openFPGA-Defender</a></td>
        <td>Defender</td>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-Defender/releases">v0.9.0</a></td>
        <td>2024-03-05</td>
        <td>obsidian-dot-dev</td>
      </tr>
      <tr>
        <td><a href="https://github.com/rolandking/openFPGA-jailbreak">openFPGA-jailbreak</a></td>
        <td>Jailbreak</td>
        <td><a href="https://github.com/rolandking/openFPGA-jailbreak/releases">v1.1</a></td>
        <td>2024-02-25</td>
        <td>rolandking</td>
      </tr>
      <tr>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-Vectrex">openFPGA-Vectrex</a></td>
        <td>Vectrex</td>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-Vectrex/releases">v0.9.0</a></td>
        <td>2024-02-18</td>
        <td>obsidian-dot-dev</td>
      </tr>
      <tr>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-Druaga">openFPGA-Druaga</a></td>
        <td>Tower of Druaga</td>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-Druaga/releases">v0.9.2</a></td>
        <td>2024-02-15</td>
        <td>obsidian-dot-dev</td>
      </tr>
      <tr>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-asteroidsdeluxe">openFPGA-asteroidsdeluxe</a></td>
        <td>Asteroids Deluxe</td>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-asteroidsdeluxe/releases">v0.9.0</a></td>
        <td>2024-02-12</td>
        <td>obsidian-dot-dev</td>
      </tr>
      <tr>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-DonkeyKongJunior">openFPGA-DonkeyKongJunior</a></td>
        <td>Donkey Kong Jr.</td>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-DonkeyKongJunior/releases">v0.9.0</a></td>
        <td>2024-02-12</td>
        <td>obsidian-dot-dev</td>
      </tr>
      <tr>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-MarioBros">openFPGA-MarioBros</a></td>
        <td>Mario Bros</td>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-MarioBros/releases">v0.9.0</a></td>
        <td>2024-02-12</td>
        <td>obsidian-dot-dev</td>
      </tr>
      <tr>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-DonkeyKong3">openFPGA-DonkeyKong3</a></td>
        <td>Donkey Kong 3</td>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-DonkeyKong3/releases">v0.9.0</a></td>
        <td>2024-02-05</td>
        <td>obsidian-dot-dev</td>
      </tr>
      <tr>
        <td><a href="https://github.com/budude2/openfgpa-GBC">openfgpa-GBC</a></td>
        <td>Game Boy</td>
        <td><a href="https://github.com/budude2/openfgpa-GBC/releases">v1.0.6</a></td>
        <td>2024-01-16</td>
        <td>budude2</td>
      </tr>
      <tr>
        <td><a href="https://github.com/budude2/openfgpa-GBC">openfgpa-GBC</a></td>
        <td>Game Boy Color</td>
        <td><a href="https://github.com/budude2/openfgpa-GBC/releases">v1.0.6</a></td>
        <td>2024-01-16</td>
        <td>budude2</td>
      </tr>
      <tr>
        <td><a href="https://github.com/AwesomeDolphin/openFPGA-SpaceInvaders">openFPGA-SpaceInvaders</a></td>
        <td>Space Invaders</td>
        <td><a href="https://github.com/AwesomeDolphin/openFPGA-SpaceInvaders/releases">1.0.0</a></td>
        <td>2023-12-25</td>
        <td>AwesomeDolphin</td>
      </tr>
      <tr>
        <td><a href="https://github.com/RndMnkIII/PocketAlphaMission">PocketAlphaMission</a></td>
        <td>ASO - Armored Scrum Object</td>
        <td><a href="https://github.com/RndMnkIII/PocketAlphaMission/releases">alphamission-pocket-v1.0</a></td>
        <td>2023-11-09</td>
        <td>RndMnkIII</td>
      </tr>
      <tr>
        <td><a href="https://github.com/somhi/FlappyBird">FlappyBird</a></td>
        <td>Flappy Bird</td>
        <td><a href="https://github.com/somhi/FlappyBird/releases">v1.0.1</a></td>
        <td>2023-07-05</td>
        <td>somhi</td>
      </tr>
      <tr>
        <td><a href="https://github.com/antongale/arcade-taitosj">arcade-taitosj</a></td>
        <td>Taito System SJ</td>
        <td><a href="https://github.com/antongale/arcade-taitosj/releases">0.1.0</a></td>
        <td>2023-06-30</td>
        <td>antongale</td>
      </tr>
      <tr>
        <td><a href="https://github.com/agg23/fpga-gameandwatch">fpga-gameandwatch</a></td>
        <td>Game and Watch</td>
        <td><a href="https://github.com/agg23/fpga-gameandwatch/releases">0.1.0</a></td>
        <td>2023-06-26</td>
        <td>agg23</td>
      </tr>
      <tr>
        <td><a href="https://github.com/agg23/fpga-tamagotchi">fpga-tamagotchi</a></td>
        <td>Tamagotchi</td>
        <td><a href="https://github.com/agg23/fpga-tamagotchi/releases">1.0.0</a></td>
        <td>2023-04-27</td>
        <td>agg23</td>
      </tr>
      <tr>
        <td><a href="https://github.com/Mazamars312/openfpga-pcengine-cd">openfpga-pcengine-cd</a></td>
        <td>PC Engine CD</td>
        <td><a href="https://github.com/Mazamars312/openfpga-pcengine-cd/releases">Mazamars312.PCECD.0.1.7-15/02/2023</a></td>
        <td>2023-02-15</td>
        <td>Mazamars312</td>
      </tr>
      <tr>
        <td><a href="https://github.com/antongale/arcade-performan">arcade-performan</a></td>
        <td>Performan</td>
        <td><a href="https://github.com/antongale/arcade-performan/releases">0.1.1</a></td>
        <td>2023-01-22</td>
        <td>antongale</td>
      </tr>
      <tr>
        <td><a href="https://github.com/agg23/openfpga-wonderswan">openfpga-wonderswan</a></td>
        <td>WonderSwan Color</td>
        <td><a href="https://github.com/agg23/openfpga-wonderswan/releases">1.0.0</a></td>
        <td>2023-01-15</td>
        <td>agg23</td>
      </tr>
      <tr>
        <td><a href="https://github.com/Mazamars312/Analogue_Pocket_Neogeo_Overdrive">Analogue_Pocket_Neogeo_Overdrive</a></td>
        <td>Neo Geo</td>
        <td><a href="https://github.com/Mazamars312/Analogue_Pocket_Neogeo_Overdrive/releases">Mazamars312.NeogeoOverdrive.0.8.0-14/01/2023</a></td>
        <td>2023-01-14</td>
        <td>Mazamars312</td>
      </tr>
      <tr>
        <td><a href="https://github.com/antongale/arcade-slapfight">arcade-slapfight</a></td>
        <td>Slap Fight</td>
        <td><a href="https://github.com/antongale/arcade-slapfight/releases">0.1.3</a></td>
        <td>2023-01-05</td>
        <td>antongale</td>
      </tr>
      <tr>
        <td><a href="https://github.com/Mazamars312/Analogue-Amiga">Analogue-Amiga</a></td>
        <td>Amiga 500</td>
        <td><a href="https://github.com/Mazamars312/Analogue-Amiga/releases">Mazamars312.Amiga.500.0.0.6-Alpha</a></td>
        <td>2022-12-10</td>
        <td>Mazamars312</td>
      </tr>
      <tr>
        <td><a href="https://github.com/antongale/arcade-exerion">arcade-exerion</a></td>
        <td>Exerion</td>
        <td><a href="https://github.com/antongale/arcade-exerion/releases">0.1.5</a></td>
        <td>2022-11-17</td>
        <td>antongale</td>
      </tr>
      <tr>
        <td><a href="https://github.com/opengateware/arcade-congo">arcade-congo</a></td>
        <td>Congo Bongo</td>
        <td><a href="https://github.com/opengateware/arcade-congo/releases">0.1.1</a></td>
        <td>2022-11-16</td>
        <td>opengateware</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-VideoBrain">openFPGA-VideoBrain</a></td>
        <td>VideoBrain Family Computer</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-VideoBrain/releases">1.0.0</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Supervision">openFPGA-Supervision</a></td>
        <td>Supervision</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Supervision/releases">1.0.0</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Super-GB">openFPGA-Super-GB</a></td>
        <td>Super Gameboy</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Super-GB/releases">1.0.3</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Studio-2">openFPGA-Studio-2</a></td>
        <td>Studio II</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Studio-2/releases">1.0.2</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Odyssey-2">openFPGA-Odyssey-2</a></td>
        <td>Odyssey 2</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Odyssey-2/releases">1.0.2</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Intellivision">openFPGA-Intellivision</a></td>
        <td>Intellivision</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Intellivision/releases">1.0.2</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Megaduck">openFPGA-Megaduck</a></td>
        <td>Mega Duck</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Megaduck/releases">1.0.2</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Genesis">openFPGA-Genesis</a></td>
        <td>Genesis</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Genesis/releases">1.0.8</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Game-King">openFPGA-Game-King</a></td>
        <td>GameKing</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Game-King/releases">1.0.3</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Gamate">openFPGA-Gamate</a></td>
        <td>Gamate</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Gamate/releases">1.0.2</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Creativision">openFPGA-Creativision</a></td>
        <td>CreatiVision</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Creativision/releases">1.0.2</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Coleco">openFPGA-Coleco</a></td>
        <td>ColecoVision</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Coleco/releases">1.0.2</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Channel-F">openFPGA-Channel-F</a></td>
        <td>Channel F</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Channel-F/releases">1.0.0</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-7800">openFPGA-7800</a></td>
        <td>Atari 7800 ProSystem</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-7800/releases">1.0.2</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Arcadia">openFPGA-Arcadia</a></td>
        <td>Arcadia 2001</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Arcadia/releases">1.0.2</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Adventure-Vision">openFPGA-Adventure-Vision</a></td>
        <td>Adventure Vision</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Adventure-Vision/releases">1.0.2</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-2600">openFPGA-2600</a></td>
        <td>Atari VCS (2600)</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-2600/releases">1.0.0</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/agg23/openfpga-pokemonmini">openfpga-pokemonmini</a></td>
        <td>Pokemon Mini</td>
        <td><a href="https://github.com/agg23/openfpga-pokemonmini/releases">0.1.1</a></td>
        <td>2022-11-05</td>
        <td>agg23</td>
      </tr>
      <tr>
        <td><a href="https://github.com/ericlewis/openFPGA-RadarScope">openFPGA-RadarScope</a></td>
        <td>Radar Scope</td>
        <td><a href="https://github.com/ericlewis/openFPGA-RadarScope/releases">0.0.1</a></td>
        <td>2022-11-02</td>
        <td>ericlewis</td>
      </tr>
      <tr>
        <td><a href="https://github.com/agg23/openfpga-pcengine">openfpga-pcengine</a></td>
        <td>PC Engine</td>
        <td><a href="https://github.com/agg23/openfpga-pcengine/releases">0.2.1</a></td>
        <td>2022-10-27</td>
        <td>agg23</td>
      </tr>
      <tr>
        <td><a href="https://github.com/ericlewis/openfpga-qbert">openfpga-qbert</a></td>
        <td>Q*Bert</td>
        <td><a href="https://github.com/ericlewis/openfpga-qbert/releases">1.1.0</a></td>
        <td>2022-10-17</td>
        <td>ericlewis</td>
      </tr>
      <tr>
        <td><a href="https://github.com/opengateware/console-supervision">console-supervision</a></td>
        <td>Supervision</td>
        <td><a href="https://github.com/opengateware/console-supervision/releases">0.1.1</a></td>
        <td>2022-10-15</td>
        <td>opengateware</td>
      </tr>
      <tr>
        <td><a href="https://github.com/ericlewis/openFPGA-DonkeyKong">openFPGA-DonkeyKong</a></td>
        <td>Donkey Kong</td>
        <td><a href="https://github.com/ericlewis/openFPGA-DonkeyKong/releases">0.0.1</a></td>
        <td>2022-10-12</td>
        <td>ericlewis</td>
      </tr>
      <tr>
        <td><a href="https://github.com/ericlewis/openfpga-asteroids">openfpga-asteroids</a></td>
        <td>Asteroids</td>
        <td><a href="https://github.com/ericlewis/openfpga-asteroids/releases">1.0.1</a></td>
        <td>2022-10-11</td>
        <td>ericlewis</td>
      </tr>
      <tr>
        <td><a href="https://github.com/ericlewis/openfpga-spacerace">openfpga-spacerace</a></td>
        <td>Space Race</td>
        <td><a href="https://github.com/ericlewis/openfpga-spacerace/releases">1.0.1</a></td>
        <td>2022-10-11</td>
        <td>ericlewis</td>
      </tr>
      <tr>
        <td><a href="https://github.com/agg23/openfpga-SNES">openfpga-SNES</a></td>
        <td>SNES</td>
        <td><a href="https://github.com/agg23/openfpga-SNES/releases">0.3.2</a></td>
        <td>2022-10-10</td>
        <td>agg23</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-NES">openFPGA-NES</a></td>
        <td>NES</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-NES/releases">1.0.0</a></td>
        <td>2022-10-10</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/opengateware/arcade-bankp">arcade-bankp</a></td>
        <td>Bank Panic</td>
        <td><a href="https://github.com/opengateware/arcade-bankp/releases">0.1.0</a></td>
        <td>2022-10-09</td>
        <td>opengateware</td>
      </tr>
      <tr>
        <td><a href="https://github.com/ericlewis/openfpga-genesis">openfpga-genesis</a></td>
        <td>Genesis</td>
        <td><a href="https://github.com/ericlewis/openfpga-genesis/releases">0.4.2</a></td>
        <td>2022-10-09</td>
        <td>ericlewis</td>
      </tr>
      <tr>
        <td><a href="https://github.com/agg23/openfpga-NES">openfpga-NES</a></td>
        <td>NES</td>
        <td><a href="https://github.com/agg23/openfpga-NES/releases">0.2.0</a></td>
        <td>2022-10-07</td>
        <td>agg23</td>
      </tr>
      <tr>
        <td><a href="https://github.com/opengateware/arcade-pooyan">arcade-pooyan</a></td>
        <td>Pooyan</td>
        <td><a href="https://github.com/opengateware/arcade-pooyan/releases">0.1.0</a></td>
        <td>2022-10-06</td>
        <td>opengateware</td>
      </tr>
      <tr>
        <td><a href="https://github.com/opengateware/arcade-gberet">arcade-gberet</a></td>
        <td>Green Beret</td>
        <td><a href="https://github.com/opengateware/arcade-gberet/releases">0.1.1</a></td>
        <td>2022-10-03</td>
        <td>opengateware</td>
      </tr>
      <tr>
        <td><a href="https://github.com/opengateware/arcade-xevious">arcade-xevious</a></td>
        <td>Xevious</td>
        <td><a href="https://github.com/opengateware/arcade-xevious/releases">0.1.0</a></td>
        <td>2022-09-25</td>
        <td>opengateware</td>
      </tr>
      <tr>
        <td><a href="https://github.com/opengateware/arcade-digdug">arcade-digdug</a></td>
        <td>Dig Dug</td>
        <td><a href="https://github.com/opengateware/arcade-digdug/releases">0.1.0</a></td>
        <td>2022-09-20</td>
        <td>opengateware</td>
      </tr>
      <tr>
        <td><a href="https://github.com/Mazamars312/Analogue_Pocket_Neogeo">Analogue_Pocket_Neogeo</a></td>
        <td>Neo Geo</td>
        <td><a href="https://github.com/Mazamars312/Analogue_Pocket_Neogeo/releases">Alpha_0.7.5_2022-19-09</a></td>
        <td>2022-09-18</td>
        <td>Mazamars312</td>
      </tr>
      <tr>
        <td><a href="https://github.com/ericlewis/openfpga-dominos">openfpga-dominos</a></td>
        <td>Dominos</td>
        <td><a href="https://github.com/ericlewis/openfpga-dominos/releases">0.0.1</a></td>
        <td>2022-09-17</td>
        <td>ericlewis</td>
      </tr>
      <tr>
        <td><a href="https://github.com/opengateware/arcade-galaga">arcade-galaga</a></td>
        <td>Galaga</td>
        <td><a href="https://github.com/opengateware/arcade-galaga/releases">v0.1.0</a></td>
        <td>2022-09-15</td>
        <td>opengateware</td>
      </tr>
      <tr>
        <td><a href="https://github.com/ericlewis/openfpga-superbreakout">openfpga-superbreakout</a></td>
        <td>Super Breakout</td>
        <td><a href="https://github.com/ericlewis/openfpga-superbreakout/releases">0.0.1</a></td>
        <td>2022-09-14</td>
        <td>ericlewis</td>
      </tr>
      <tr>
        <td><a href="https://github.com/ericlewis/openfpga-lunarlander">openfpga-lunarlander</a></td>
        <td>Lunar Lander</td>
        <td><a href="https://github.com/ericlewis/openfpga-lunarlander/releases">0.9.1</a></td>
        <td>2022-09-13</td>
        <td>ericlewis</td>
      </tr>
      <tr>
        <td><a href="https://github.com/nullobject/openfpga-tecmo">openfpga-tecmo</a></td>
        <td>Tecmo</td>
        <td><a href="https://github.com/nullobject/openfpga-tecmo/releases">v2.0</a></td>
        <td>2022-09-10</td>
        <td>nullobject</td>
      </tr>
      <tr>
        <td><a href="https://github.com/agg23/openfpga-pong">openfpga-pong</a></td>
        <td>Pong</td>
        <td><a href="https://github.com/agg23/openfpga-pong/releases">1.2.0</a></td>
        <td>2022-09-09</td>
        <td>agg23</td>
      </tr>
      <tr>
        <td><a href="https://github.com/agg23/openfpga-arduboy">openfpga-arduboy</a></td>
        <td>Arduboy</td>
        <td><a href="https://github.com/agg23/openfpga-arduboy/releases">0.9.0</a></td>
        <td>2022-09-03</td>
        <td>agg23</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-SG1000">openFPGA-SG1000</a></td>
        <td>SG-1000</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-SG1000/releases">v1.2.0</a></td>
        <td>2022-08-27</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-SMS">openFPGA-SMS</a></td>
        <td>Master System</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-SMS/releases">v1.2.0</a></td>
        <td>2022-08-27</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-GG">openFPGA-GG</a></td>
        <td>Game Gear</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-GG/releases">v1.3.0</a></td>
        <td>2022-08-27</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-GBA">openFPGA-GBA</a></td>
        <td>Game Boy Advance</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-GBA/releases">v1.2.0</a></td>
        <td>2022-08-27</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-GB-GBC">openFPGA-GB-GBC</a></td>
        <td>Game Boy</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-GB-GBC/releases">v1.3.0</a></td>
        <td>2022-08-27</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-GB-GBC">openFPGA-GB-GBC</a></td>
        <td>Game Boy Color</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-GB-GBC/releases">v1.3.0</a></td>
        <td>2022-08-27</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spacemen3/PDP-1">PDP-1</a></td>
        <td>PDP-1: Spacewar!</td>
        <td><a href="https://github.com/spacemen3/PDP-1/releases">v1.1.0</a></td>
        <td>2022-08-23</td>
        <td>spacemen3</td>
      </tr>
  </tbody>
</table>