import masterRepoList from '../../data/masterRepoList.json' assert { type: 'json' };
import { writeFileSync } from 'node:fs';
import { dirname, join } from 'node:path';
import { fileURLToPath } from 'url';
import chalk from 'chalk';
import { statusEmo } from '../../common/helpers.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const sortByDateDesc = (cores) => {
  return cores.sort((a, b) => {
    const aDate = new Date(a.publishDate);
    const bDate = new Date(b.publishDate);

    if (aDate > bDate) return -1;
    if (aDate < bDate) return 1;
    return 0;
  });
};

const tableTop = `
<table>
  <thead>
    <tr>
      <th>Core</th>
      <th>Platform</th>
      <th>Latest</th>
      <th>Date Updated</th>
      <th>Developer</th>
    </tr>
  </thead>
  <tbody>`;

const tableBottom = `
  </tbody>
</table>`;

export const generateTable = function (updatedMasterList) {
  const allCoresData = sortByDateDesc(updatedMasterList || masterRepoList).map((core) => {
    const repoUrlSplit = core.url.split('/');
    const pDateSplit = core.publishDate.split('T');

    return `
      <tr>
        <td><a href="${core.url}">${repoUrlSplit[repoUrlSplit.length - 1]}</a></td>
        <td>${core.platform}</td>
        <td><a href="${core.url}/releases">${core.release}</a></td>
        <td>${pDateSplit[0]}</td>
        <td>${core.developer}</td>
      </tr>`;
  });
  const fullTable = `${tableTop}${allCoresData.join('')}${tableBottom}`;
  writeFileSync(join(__dirname, 'readmeCoresTable.md'), fullTable, 'utf-8');
  console.log(chalk.cyan(`${statusEmo.info} Table written for readme.`));
};
