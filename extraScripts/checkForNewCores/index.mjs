import { updateMasterRepoList } from '../../common/updateMasterRepoList.mjs';

(async function () {
  await updateMasterRepoList();
})();
