import chalk from 'chalk';
import { downloadCore } from '../../common/downloadCore.mjs';
import { deleteNewCoresDir } from '../../common/deleteNewCoresDir.mjs';
import masterList from '../../data/masterRepoList.json' assert { type: 'json' };
import config from '../../config.json' assert { type: 'json' };
import { statusEmo } from '../../common/helpers.mjs';

(async function () {
  if (process.env.GITHUB_KEY || config.personalAccessToken) {
    await deleteNewCoresDir();
    for (let i = 0; i < masterList.length; i++) {
      const core = masterList[i];
      try {
        await downloadCore(core, `${statusEmo.fire} Failed to download ${core.id} core.`);
      } catch (error) {
        console.log(chalk.red.bold(`${statusEmo.fire} Error downloading ${core.id} core.`));
      }
    }
  } else {
    console.log(
      chalk.yellow.bold(
        // eslint-disable-next-line
        `${statusEmo.info} You can't automatically download cores without a GitHub personal access token.`
      )
    );
  }
})();
