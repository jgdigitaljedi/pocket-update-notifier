import { whitespaceRemoveBreaks, whitespaceSingleSpace } from 'stringman-utils';
import fetch from 'node-fetch';

export const formatApiResult = (apiData, githubData) => {
  const data = githubData?.data[0] || null;
  const url = `https://${apiData.repository.platform}.com/${apiData.repository.owner}/${apiData.repository.name}`;
  return {
    release: apiData.release?.tag_name || apiData.prerelease.tag_name,
    publishDate: apiData.release?.release_date || apiData.prerelease?.release_date,
    url: url,
    description: data?.body ? whitespaceSingleSpace(whitespaceRemoveBreaks(data.body)) : '',
    download: data ? data?.assets[0].browser_download_url : `${url}/releases`,
    id: apiData.identifier,
    platform: apiData.release?.platform.name || apiData.prerelease?.platform.name,
    developer: apiData.repository.owner,
    repo: apiData.repository.name
  };
};

export const getUrlFromCore = (core) => {
  if (core.repository.platform === 'github') {
    return `https://github.com/${core.repository.owner}/${core.repository.name}`;
  }
};

export const getApiData = async () => {
  const corePage = await fetch(
    'https://openfpga-cores-inventory.github.io/analogue-pocket/api/v1/cores.json',
    {
      method: 'GET',
      headers: {
        host: 'openfpga-cores-inventory.github.io',
        Accept: 'application/json'
      }
    }
  );
  const coreInfo = await corePage.json();
  return coreInfo.data;
};

export const statusEmo = {
  check: '\u{2705}',
  fire: '\u{1F525}',
  ex: '\u{274C}',
  package: '\u{1F4E6}',
  game: '\u{1F3AE}',
  up: '\u{2B06}',
  down: '\u{2B07}',
  trash: '\u{1F5D1}',
  info: '\u{1F6C8}'
};
