import fetch from 'node-fetch';
import * as cheerio from 'cheerio';
import myFwVersion from '../data/myPocketFirmware.json' assert { type: 'json' };
import chalk from 'chalk';
import { statusEmo } from './helpers.mjs';

export async function getPocketFirmwareVersion() {
  const pocketPage = await fetch('https://www.analogue.co/support/pocket/firmware');
  const body = await pocketPage.text();
  const $ = cheerio.load(body);
  const className = $('[class*=firmwares_version__]').attr('class');
  const container = $(`.${className} > p`);
  const filtered = Array.from(container).filter((item, index) => index === 0)[0];
  const latest = $(filtered).text();
  if (myFwVersion.myCurrentVersion !== latest) {
    const link = $('a[class*=new_button__]').attr('href');
    console.log(
      chalk.red.bold(
        `${statusEmo.up} POCKET FIRMWARE VERSION UPDATE AVAILABLE (${latest}): ${link}`
      )
    );
  } else {
    console.log(chalk.green(`${statusEmo.check} Your pocket firmware is currently up to date.`));
  }
  return latest;
}
