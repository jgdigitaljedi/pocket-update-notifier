import chalk from 'chalk';
import { writeFileSync } from 'node:fs';
import masterList from '../data/masterRepoList.json' assert { type: 'json' };
import { formatApiResult, getApiData, getUrlFromCore, statusEmo } from './helpers.mjs';
import { fileURLToPath } from 'node:url';
import { dirname, join } from 'node:path';
import { getCoreInfo } from './getGithubData.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

export const updateMasterRepoList = async () => {
  const apiData = await getApiData();
  const currentCores = masterList.map((l) => l.id) || [];
  const newCoreInfo = apiData
    .map((core) => {
      if (currentCores.indexOf(core.identifier) > -1) {
        return null;
      }
      console.log(
        chalk.red.magenta(`New ${core.platform} core available at ${getUrlFromCore(core)} !`)
      );
      return core;
    })
    .filter((core) => !!core);
  if (!newCoreInfo.length) {
    console.log(chalk.green(`${statusEmo.info} No new cores available.`));
    return masterList;
  } else {
    try {
      const newCoreList = [];
      for (let i = 0; i < newCoreInfo.length; i++) {
        const currentApiCore = newCoreInfo[i];
        const currentApiCoreCallData = {
          developer: currentApiCore.repository.owner,
          repo: currentApiCore.repository.name
        };
        const release = await getCoreInfo(currentApiCoreCallData);
        const releaseData = formatApiResult(currentApiCore, release);
        newCoreList.push(releaseData);
      }
      const updatedCoreList = [...(masterList || []), ...(newCoreList || [])];
      writeFileSync(
        join(__dirname, '../data/masterRepoList.json'),
        JSON.stringify(updatedCoreList, null, 2),
        'utf-8'
      );
      console.log(
        chalk.green.bold(
          `${statusEmo.check} Successfully updated masterRepoList with ${newCoreList
            .map((c) => c.id)
            .join(', ')} core${newCoreList.length > 1 ? 's' : ''}.`
        )
      );
      return Promise.resolve(updatedCoreList);
    } catch (err) {
      console.log(
        chalk.red(
          `${statusEmo.fire} Error updating local masterRepoList with new core information! You might run this again.`,
          err
        )
      );
      return Promise.reject(err);
    }
  }
};
