import { readdirSync } from 'node:fs';
import * as AdmZip from 'adm-zip';
import { fileURLToPath } from 'node:url';
import { dirname, extname, join } from 'node:path';
import { statusEmo } from './helpers.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

export function unzipNewCores() {
  return new Promise((resolve, reject) => {
    try {
      const zipFiles = readdirSync(join(__dirname, '../newCores'));
      const zLen = zipFiles?.length || 0;
      if (zLen) {
        for (let i = 0; i < zLen; i++) {
          const file = zipFiles[i];
          if (extname(file) === '.zip') {
            const coreDir = file.split('.')[0];
            const zip = new AdmZip.default(join(__dirname, `../newCores/${file}`));
            zip.extractAllTo(join(__dirname, `../newCores/${coreDir}`), true, (error) => {
              if (error) {
                reject(`${statusEmo.fire} Error extracing core: ${error}`);
              }
            });
            resolve();
          }
        }
      } else {
        resolve();
      }
    } catch (error) {
      reject(`${statusEmo.fire} Error unzipping file: ${error}`);
    }
  });
}
