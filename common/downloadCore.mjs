import { pipeline } from 'node:stream';
import { promisify } from 'node:util';
import fetch from 'node-fetch';
import chalk from 'chalk';
import { createWriteStream } from 'node:fs';
import { dirname, join } from 'node:path';
import { fileURLToPath } from 'node:url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

export async function downloadCore(core, errorMessage) {
  const streamPipeline = promisify(pipeline);
  const response = await fetch(core.download);
  if (!response.ok) {
    console.log(chalk.red(errorMessage));
    return Promise.resolve();
  }
  await streamPipeline(
    response.body,
    createWriteStream(join(__dirname, `../newCores/${core.id}.zip`))
  );
}
