import { Octokit } from 'octokit';
import config from '../config.json' assert { type: 'json' };

const octokit = new Octokit({
  auth: config.personalAccessToken || process.env.GITHUB_KEY
});

export async function getCoreInfo(core) {
  if (config.personalAccessToken || process.env.GITHUB_KEY) {
    return octokit.request('GET /repos/{owner}/{repo}/releases', {
      owner: core.developer,
      repo: core.repo
    });
  } else {
    return Promise.resolve(null);
  }
}
