import { dirname, extname, join } from 'node:path';
import { readdirSync, rmSync, unlinkSync } from 'node:fs';
import { fileURLToPath } from 'node:url';
import { statusEmo } from './helpers.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
const dir = readdirSync(join(`${__dirname}`, '../newCores')) || [];

export async function deleteZipFiles() {
  const zipFiles = dir.filter((file) => extname(file) === '.zip') || [];
  const zLen = zipFiles.length;
  for (let i = 0; i < zLen; i++) {
    unlinkSync(join(__dirname, '../newCores', zipFiles[i]), (err) => {
      if (err) {
        console.log(`${statusEmo.fire} ${err}`);
      }
    });
    if (i + 1 === zLen) return Promise.resolve();
  }
}

export async function deleteSubdirs() {
  const directories = dir.filter((obj) => extname(obj) !== '.zip' && obj !== '.gitkeep') || [];
  const dLen = directories.length;
  for (let i = 0; i < dLen; i++) {
    rmSync(
      join(__dirname, '../newCores', directories[i]),
      { recursive: true, force: true },
      (err) => {
        if (err) console.log(`${statusEmo.fire} ${err}`);
      }
    );
    if (i + i === dLen) return Promise.resolve();
  }
}

export async function deleteNewCoresDir() {
  await deleteZipFiles();
  await deleteSubdirs();
}
