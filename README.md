# Pocket Update Notifier

This is a tool to look for openFPGA core and firmware updates for your Analogue Pocket. As more and more cores come out and the existing cores and the firmware receive updates, it has become more and more difficult to track what's available and it there is a newer version of a core and the firmware you are using. This tool aims to resolve that!

This website has proven to be quite reliable for tracking cores: https://joshcampbell191.github.io/openfpga-cores-inventory/analogue-pocket.html . In fact, the developer created [an API](https://github.com/joshcampbell191/openfpga-cores-inventory/blob/master/_docs/v1.md) which I have since refactored these scripts to use which will keep the `masterRepoList.json` file updated for you.

![Pocket Update Notifier running in terminal](pocketUpdater.png)

---

## What it does

- all data is stored inside of JSON files
- checks your cores and currently installed versions you put inside of the `data/myCores.json` file
  - if an update is available, it will let you know in the console, download and extract the core to the `newCores` directory, and delete the zip file
  - if an update for a core is not available (you currently have the latest version), it will let you know in the console that this is the case
- checks Analogue support website for the latest version of the Pocket firmware available and compares it to the version you have in the `myPocketFirmware.json` file
  - if an update is available, it will let you know in the console (I don't want to be responsible for the possible of a corrupt firmware download so it does not download the firmware)
  - if your firmware is the latest version, it will let you know that as well
- writes to `data/updatedMyCores.json` which can serve as your new `data/myCores.json` once you've installed the newest versions of the downloaded cores to your Pocket. You can either manually copy and paste the contents of `data/updatedMyCores.json` to your `data/myCores.json` file, or you can simply run `npm run updateMyCores` to do this automatically.
- contains additional scripts mentioned near the bottom of this readme to download all available cores, delete cores from your `newCores` folder, and fetch information on every core in the `data/masterRepoList.json` file.
- checks the API built by the same developer that created [this site](https://joshcampbell191.github.io/openfpga-cores-inventory/analogue-pocket.html) to see if any new cores are available
  - if there are new cores, you will see a red message in the console about it
  - if there are no new cores, you will see a green message in the console about it

---

## Requirements

You must have [Node.js](https://nodejs.org/en/) version 16 or higher installed because this script uses ESM imports and type assertions on imports.

I have a .nvmrc file in the repo that is set to use gallium, so if you are using [nvm](https://github.com/nvm-sh/nvm) and have a script setup to automatically change you node version then this will just work.

---

## Installation

```
git clone https://gitlab.com/jgdigitaljedi/pocket-update-notifier.git
cd pocket-update-notifier
npm i
```

---

## Usage

First, you need to decide if you want it to download and extract the cores for you automatically. If you want this to happen, you need a GitHub personal access token. If you don't provide a GitHub personal access token this tool will still work, but instead of automatically downloading and extracting cores, it will give you text in the console that you have an update and will provide a link to the repository where you can download the core manually.

- Either set a `GITHUB_KEY` environment variable to equal your GitHub personal access token, or simply add it to the value of `personalAccessToken` in the `config.json` file in the root of the project. When this config option is an empty string, it will check for the environment variable instead. [GitHub - Creating a personal access token](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token)

Next, add you firmware version to `data/myPocketFirmware.json`. It should be formatted exactly how it is on the Analogue Pocket support page as the script that checks for firmware updates actually scrapes [this page](https://www.analogue.co/support/pocket/firmware).

Next, you should setup your `data/myCores.json` file as this is used to compare your version of each core to the latest version available. I'm using this script myself, so I've got this file filled with my current core setup. You can delete this and create an array of objects with the following properties:

- id: the id of the core from `data/masterRepoList.json`
- myVersion: the version of this core that you are currently using

example:

```
[
  {
    "id": "Mazamars312.NeoGeo",
    "myVersion": "Alpha_0.7.5_2022-19-09"
  },
  {
    "id": "ericlewis.Genesis",
    "myVersion": "0.4.2"
  },
  {
    "id": "nullobject.tecmo",
    "myVersion": "v2.0"
  },
  {
    "id": "Spiritualized.NES",
    "myVersion": "1.0.0"
  }
  ...
]
```

The command to run the script has been put into the package.json file as the "start" script, so all you have to do is run:

```
npm start
```

---

## Configuration

A JSON file is used as a configuration file to control what this tool does for you. If you want to change the behavior of the tool, you can do so in `config.json` with the following options:

- **deleteNewCoresBeforeRun** _(default: true)_: Delete zip files and extracted directories inside of `newCores` before running the script.
- **downloadUpdates** _(default: true)_: Download zip files of any cores that need to be updated to `data/newCores`. _requires GitHub personal access token to work_
- **extractUpdates** _(default: true)_: Extract downloaded zips inside of `newCores`. Note that this is dependent on `downloadUpdates` so, if it is set to `false`, this option will have no effect. _requires GitHub personal access token to work_
- **deleteZips** _(default: true)_: Try to delete downloaded zip files after they are extracted. Note that this option is dependent on both `downloadUpdates` and `extractUpdates` so, if either of them are set to `false`, this option will have no effect.
- **writeNewCoresFile** _(default: true)_: Write to `data/updatedMyCores.json` with latest versions of your `data/myCores.json` so you can easily update your `data/myCores.json` file after you've updated the cores on your Pocket. Note that if this option is set to `false` then the `npm run updateMyCores` script will have no meaningful effect.
- **checkFwUpdates** _(default: true)_: Check for Pocket firmware updates on Analogue's support page.
- **personalAccessToken** _(default: "")_: If you don't want to put your GitHub personal access token into your environment variables, you can simply add it to this property in the config. When this is left blank, it will try to use your `GITHUB_KEY` environment variable instead.
- **checkForNewCores** _(default: true)_: Check for new cores listed on [this page](https://joshcampbell191.github.io/openfpga-cores-inventory/analogue-pocket.html) that aren't yet in the `data/masterRepoList.json` file
- **microSdPath** _(default: "")_: This is only needed if you plan to run the `readMyCores` script and should contain the absolute path to your Analogue Pocket's micro SD card when plugged into your computer.

---

### Run anywhere

You can just run this tool in the terminal from the project directory. If you want to make it even more convenient to use, create an alias for it so you can run it with a single command in the terminal from any directory. It will look something like this in you .bashrc, .zshrc, etc.

```
export alias pocketUpdates='node ~/code/pocket-update-notifier/index.mjs'
```

Once you have that added unsing the correct path for your unique setup and directory structure, just run `source ~/.zshrc` (or .bashrc, etc) to get your new alias into your terminal and you should be able to run this from anywhere. I'm on Linux and this is how it works, but I am pretty sure it is the same process on Mac. Windows users...I have no clue how this works for you. You might be able to do it using a PATH variable?

---

## Results

This script will log several things to the console as it runs.

- You will get success/failure logs for each initial core repo request. This indicates whether or not the repo data was fetched.
- If there are newer versions of the cores you are using, you will then see a console log for each core with details about which core has a newer version, the link to download the new version, and whether or not the script could successfully download the new version for you of you have provided a GitHub personal access token.
  - If the new version download fails, you can at least use the link in the console to download it yourself.
- If all of your current cores are up to date, you will see a console log message that says `Your cores are currently up to date.`
- if your Analogue Pocket firmware is up to date, you'll receive a message letting you know. If not, you'll receive a bold, red message telling you there is an update and providing you with a link to download the latest firmware from your browser. (I'm not downloading the firmware automatically because I would hate to be responsible for a corrupt firmware download.)
- All error messages will be logged and can include:
  - error when failed to fetch core info from github
  - error when failed to download core
  - error when failed to extract core
  - error when failed to delete `newCores` directory at start of script

When completed, it will write to the following files:

- **coreUpdates.json** => This file will contain information about the cores you are using that have updates available.
- **updatedMyCores.json** => This file will contain an array of objects of what your `myCores.json` file would look like once you've updated all of your cores. The idea is that after you have updated the cores on you Analogue Pocket, you can simply copy the contents of this file into `myCores.json` to have a newly updated list so you do not receive duplicate results the next time you run the script.
- **newCores** => This directory will contain the downloaded zip files and extracted folder (as long as there are no errors) of any of the cores you have that need to be updated. Simply copy extracted cores to your Analogue Pocket accordingly, unzip and copy if an error occured during extraction, or use the logged links to download your cores if the downloads failed.
- profit!

---

## Cores

<table>
  <thead>
    <tr>
      <th>Core</th>
      <th>Platform</th>
      <th>Latest</th>
      <th>Date Updated</th>
      <th>Developer</th>
    </tr>
  </thead>
  <tbody>
      <tr>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-Robotron">openFPGA-Robotron</a></td>
        <td>Robotron</td>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-Robotron/releases">v0.9.1</a></td>
        <td>2024-03-09</td>
        <td>obsidian-dot-dev</td>
      </tr>
      <tr>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-Defender">openFPGA-Defender</a></td>
        <td>Defender</td>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-Defender/releases">v0.9.0</a></td>
        <td>2024-03-05</td>
        <td>obsidian-dot-dev</td>
      </tr>
      <tr>
        <td><a href="https://github.com/rolandking/openFPGA-jailbreak">openFPGA-jailbreak</a></td>
        <td>Jailbreak</td>
        <td><a href="https://github.com/rolandking/openFPGA-jailbreak/releases">v1.1</a></td>
        <td>2024-02-25</td>
        <td>rolandking</td>
      </tr>
      <tr>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-Vectrex">openFPGA-Vectrex</a></td>
        <td>Vectrex</td>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-Vectrex/releases">v0.9.0</a></td>
        <td>2024-02-18</td>
        <td>obsidian-dot-dev</td>
      </tr>
      <tr>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-Druaga">openFPGA-Druaga</a></td>
        <td>Tower of Druaga</td>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-Druaga/releases">v0.9.2</a></td>
        <td>2024-02-15</td>
        <td>obsidian-dot-dev</td>
      </tr>
      <tr>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-asteroidsdeluxe">openFPGA-asteroidsdeluxe</a></td>
        <td>Asteroids Deluxe</td>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-asteroidsdeluxe/releases">v0.9.0</a></td>
        <td>2024-02-12</td>
        <td>obsidian-dot-dev</td>
      </tr>
      <tr>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-DonkeyKongJunior">openFPGA-DonkeyKongJunior</a></td>
        <td>Donkey Kong Jr.</td>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-DonkeyKongJunior/releases">v0.9.0</a></td>
        <td>2024-02-12</td>
        <td>obsidian-dot-dev</td>
      </tr>
      <tr>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-MarioBros">openFPGA-MarioBros</a></td>
        <td>Mario Bros</td>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-MarioBros/releases">v0.9.0</a></td>
        <td>2024-02-12</td>
        <td>obsidian-dot-dev</td>
      </tr>
      <tr>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-DonkeyKong3">openFPGA-DonkeyKong3</a></td>
        <td>Donkey Kong 3</td>
        <td><a href="https://github.com/obsidian-dot-dev/openFPGA-DonkeyKong3/releases">v0.9.0</a></td>
        <td>2024-02-05</td>
        <td>obsidian-dot-dev</td>
      </tr>
      <tr>
        <td><a href="https://github.com/budude2/openfgpa-GBC">openfgpa-GBC</a></td>
        <td>Game Boy</td>
        <td><a href="https://github.com/budude2/openfgpa-GBC/releases">v1.0.6</a></td>
        <td>2024-01-16</td>
        <td>budude2</td>
      </tr>
      <tr>
        <td><a href="https://github.com/budude2/openfgpa-GBC">openfgpa-GBC</a></td>
        <td>Game Boy Color</td>
        <td><a href="https://github.com/budude2/openfgpa-GBC/releases">v1.0.6</a></td>
        <td>2024-01-16</td>
        <td>budude2</td>
      </tr>
      <tr>
        <td><a href="https://github.com/AwesomeDolphin/openFPGA-SpaceInvaders">openFPGA-SpaceInvaders</a></td>
        <td>Space Invaders</td>
        <td><a href="https://github.com/AwesomeDolphin/openFPGA-SpaceInvaders/releases">1.0.0</a></td>
        <td>2023-12-25</td>
        <td>AwesomeDolphin</td>
      </tr>
      <tr>
        <td><a href="https://github.com/RndMnkIII/PocketAlphaMission">PocketAlphaMission</a></td>
        <td>ASO - Armored Scrum Object</td>
        <td><a href="https://github.com/RndMnkIII/PocketAlphaMission/releases">alphamission-pocket-v1.0</a></td>
        <td>2023-11-09</td>
        <td>RndMnkIII</td>
      </tr>
      <tr>
        <td><a href="https://github.com/somhi/FlappyBird">FlappyBird</a></td>
        <td>Flappy Bird</td>
        <td><a href="https://github.com/somhi/FlappyBird/releases">v1.0.1</a></td>
        <td>2023-07-05</td>
        <td>somhi</td>
      </tr>
      <tr>
        <td><a href="https://github.com/antongale/arcade-taitosj">arcade-taitosj</a></td>
        <td>Taito System SJ</td>
        <td><a href="https://github.com/antongale/arcade-taitosj/releases">0.1.0</a></td>
        <td>2023-06-30</td>
        <td>antongale</td>
      </tr>
      <tr>
        <td><a href="https://github.com/agg23/fpga-gameandwatch">fpga-gameandwatch</a></td>
        <td>Game and Watch</td>
        <td><a href="https://github.com/agg23/fpga-gameandwatch/releases">0.1.0</a></td>
        <td>2023-06-26</td>
        <td>agg23</td>
      </tr>
      <tr>
        <td><a href="https://github.com/agg23/fpga-tamagotchi">fpga-tamagotchi</a></td>
        <td>Tamagotchi</td>
        <td><a href="https://github.com/agg23/fpga-tamagotchi/releases">1.0.0</a></td>
        <td>2023-04-27</td>
        <td>agg23</td>
      </tr>
      <tr>
        <td><a href="https://github.com/Mazamars312/openfpga-pcengine-cd">openfpga-pcengine-cd</a></td>
        <td>PC Engine CD</td>
        <td><a href="https://github.com/Mazamars312/openfpga-pcengine-cd/releases">Mazamars312.PCECD.0.1.7-15/02/2023</a></td>
        <td>2023-02-15</td>
        <td>Mazamars312</td>
      </tr>
      <tr>
        <td><a href="https://github.com/antongale/arcade-performan">arcade-performan</a></td>
        <td>Performan</td>
        <td><a href="https://github.com/antongale/arcade-performan/releases">0.1.1</a></td>
        <td>2023-01-22</td>
        <td>antongale</td>
      </tr>
      <tr>
        <td><a href="https://github.com/agg23/openfpga-wonderswan">openfpga-wonderswan</a></td>
        <td>WonderSwan Color</td>
        <td><a href="https://github.com/agg23/openfpga-wonderswan/releases">1.0.0</a></td>
        <td>2023-01-15</td>
        <td>agg23</td>
      </tr>
      <tr>
        <td><a href="https://github.com/Mazamars312/Analogue_Pocket_Neogeo_Overdrive">Analogue_Pocket_Neogeo_Overdrive</a></td>
        <td>Neo Geo</td>
        <td><a href="https://github.com/Mazamars312/Analogue_Pocket_Neogeo_Overdrive/releases">Mazamars312.NeogeoOverdrive.0.8.0-14/01/2023</a></td>
        <td>2023-01-14</td>
        <td>Mazamars312</td>
      </tr>
      <tr>
        <td><a href="https://github.com/antongale/arcade-slapfight">arcade-slapfight</a></td>
        <td>Slap Fight</td>
        <td><a href="https://github.com/antongale/arcade-slapfight/releases">0.1.3</a></td>
        <td>2023-01-05</td>
        <td>antongale</td>
      </tr>
      <tr>
        <td><a href="https://github.com/Mazamars312/Analogue-Amiga">Analogue-Amiga</a></td>
        <td>Amiga 500</td>
        <td><a href="https://github.com/Mazamars312/Analogue-Amiga/releases">Mazamars312.Amiga.500.0.0.6-Alpha</a></td>
        <td>2022-12-10</td>
        <td>Mazamars312</td>
      </tr>
      <tr>
        <td><a href="https://github.com/antongale/arcade-exerion">arcade-exerion</a></td>
        <td>Exerion</td>
        <td><a href="https://github.com/antongale/arcade-exerion/releases">0.1.5</a></td>
        <td>2022-11-17</td>
        <td>antongale</td>
      </tr>
      <tr>
        <td><a href="https://github.com/opengateware/arcade-congo">arcade-congo</a></td>
        <td>Congo Bongo</td>
        <td><a href="https://github.com/opengateware/arcade-congo/releases">0.1.1</a></td>
        <td>2022-11-16</td>
        <td>opengateware</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-VideoBrain">openFPGA-VideoBrain</a></td>
        <td>VideoBrain Family Computer</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-VideoBrain/releases">1.0.0</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Supervision">openFPGA-Supervision</a></td>
        <td>Supervision</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Supervision/releases">1.0.0</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Super-GB">openFPGA-Super-GB</a></td>
        <td>Super Gameboy</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Super-GB/releases">1.0.3</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Studio-2">openFPGA-Studio-2</a></td>
        <td>Studio II</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Studio-2/releases">1.0.2</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Odyssey-2">openFPGA-Odyssey-2</a></td>
        <td>Odyssey 2</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Odyssey-2/releases">1.0.2</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Intellivision">openFPGA-Intellivision</a></td>
        <td>Intellivision</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Intellivision/releases">1.0.2</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Megaduck">openFPGA-Megaduck</a></td>
        <td>Mega Duck</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Megaduck/releases">1.0.2</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Genesis">openFPGA-Genesis</a></td>
        <td>Genesis</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Genesis/releases">1.0.8</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Game-King">openFPGA-Game-King</a></td>
        <td>GameKing</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Game-King/releases">1.0.3</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Gamate">openFPGA-Gamate</a></td>
        <td>Gamate</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Gamate/releases">1.0.2</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Creativision">openFPGA-Creativision</a></td>
        <td>CreatiVision</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Creativision/releases">1.0.2</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Coleco">openFPGA-Coleco</a></td>
        <td>ColecoVision</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Coleco/releases">1.0.2</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Channel-F">openFPGA-Channel-F</a></td>
        <td>Channel F</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Channel-F/releases">1.0.0</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-7800">openFPGA-7800</a></td>
        <td>Atari 7800 ProSystem</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-7800/releases">1.0.2</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Arcadia">openFPGA-Arcadia</a></td>
        <td>Arcadia 2001</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Arcadia/releases">1.0.2</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Adventure-Vision">openFPGA-Adventure-Vision</a></td>
        <td>Adventure Vision</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-Adventure-Vision/releases">1.0.2</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-2600">openFPGA-2600</a></td>
        <td>Atari VCS (2600)</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-2600/releases">1.0.0</a></td>
        <td>2022-11-08</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/agg23/openfpga-pokemonmini">openfpga-pokemonmini</a></td>
        <td>Pokemon Mini</td>
        <td><a href="https://github.com/agg23/openfpga-pokemonmini/releases">0.1.1</a></td>
        <td>2022-11-05</td>
        <td>agg23</td>
      </tr>
      <tr>
        <td><a href="https://github.com/ericlewis/openFPGA-RadarScope">openFPGA-RadarScope</a></td>
        <td>Radar Scope</td>
        <td><a href="https://github.com/ericlewis/openFPGA-RadarScope/releases">0.0.1</a></td>
        <td>2022-11-02</td>
        <td>ericlewis</td>
      </tr>
      <tr>
        <td><a href="https://github.com/agg23/openfpga-pcengine">openfpga-pcengine</a></td>
        <td>PC Engine</td>
        <td><a href="https://github.com/agg23/openfpga-pcengine/releases">0.2.1</a></td>
        <td>2022-10-27</td>
        <td>agg23</td>
      </tr>
      <tr>
        <td><a href="https://github.com/ericlewis/openfpga-qbert">openfpga-qbert</a></td>
        <td>Q*Bert</td>
        <td><a href="https://github.com/ericlewis/openfpga-qbert/releases">1.1.0</a></td>
        <td>2022-10-17</td>
        <td>ericlewis</td>
      </tr>
      <tr>
        <td><a href="https://github.com/opengateware/console-supervision">console-supervision</a></td>
        <td>Supervision</td>
        <td><a href="https://github.com/opengateware/console-supervision/releases">0.1.1</a></td>
        <td>2022-10-15</td>
        <td>opengateware</td>
      </tr>
      <tr>
        <td><a href="https://github.com/ericlewis/openFPGA-DonkeyKong">openFPGA-DonkeyKong</a></td>
        <td>Donkey Kong</td>
        <td><a href="https://github.com/ericlewis/openFPGA-DonkeyKong/releases">0.0.1</a></td>
        <td>2022-10-12</td>
        <td>ericlewis</td>
      </tr>
      <tr>
        <td><a href="https://github.com/ericlewis/openfpga-asteroids">openfpga-asteroids</a></td>
        <td>Asteroids</td>
        <td><a href="https://github.com/ericlewis/openfpga-asteroids/releases">1.0.1</a></td>
        <td>2022-10-11</td>
        <td>ericlewis</td>
      </tr>
      <tr>
        <td><a href="https://github.com/ericlewis/openfpga-spacerace">openfpga-spacerace</a></td>
        <td>Space Race</td>
        <td><a href="https://github.com/ericlewis/openfpga-spacerace/releases">1.0.1</a></td>
        <td>2022-10-11</td>
        <td>ericlewis</td>
      </tr>
      <tr>
        <td><a href="https://github.com/agg23/openfpga-SNES">openfpga-SNES</a></td>
        <td>SNES</td>
        <td><a href="https://github.com/agg23/openfpga-SNES/releases">0.3.2</a></td>
        <td>2022-10-10</td>
        <td>agg23</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-NES">openFPGA-NES</a></td>
        <td>NES</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-NES/releases">1.0.0</a></td>
        <td>2022-10-10</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/opengateware/arcade-bankp">arcade-bankp</a></td>
        <td>Bank Panic</td>
        <td><a href="https://github.com/opengateware/arcade-bankp/releases">0.1.0</a></td>
        <td>2022-10-09</td>
        <td>opengateware</td>
      </tr>
      <tr>
        <td><a href="https://github.com/ericlewis/openfpga-genesis">openfpga-genesis</a></td>
        <td>Genesis</td>
        <td><a href="https://github.com/ericlewis/openfpga-genesis/releases">0.4.2</a></td>
        <td>2022-10-09</td>
        <td>ericlewis</td>
      </tr>
      <tr>
        <td><a href="https://github.com/agg23/openfpga-NES">openfpga-NES</a></td>
        <td>NES</td>
        <td><a href="https://github.com/agg23/openfpga-NES/releases">0.2.0</a></td>
        <td>2022-10-07</td>
        <td>agg23</td>
      </tr>
      <tr>
        <td><a href="https://github.com/opengateware/arcade-pooyan">arcade-pooyan</a></td>
        <td>Pooyan</td>
        <td><a href="https://github.com/opengateware/arcade-pooyan/releases">0.1.0</a></td>
        <td>2022-10-06</td>
        <td>opengateware</td>
      </tr>
      <tr>
        <td><a href="https://github.com/opengateware/arcade-gberet">arcade-gberet</a></td>
        <td>Green Beret</td>
        <td><a href="https://github.com/opengateware/arcade-gberet/releases">0.1.1</a></td>
        <td>2022-10-03</td>
        <td>opengateware</td>
      </tr>
      <tr>
        <td><a href="https://github.com/opengateware/arcade-xevious">arcade-xevious</a></td>
        <td>Xevious</td>
        <td><a href="https://github.com/opengateware/arcade-xevious/releases">0.1.0</a></td>
        <td>2022-09-25</td>
        <td>opengateware</td>
      </tr>
      <tr>
        <td><a href="https://github.com/opengateware/arcade-digdug">arcade-digdug</a></td>
        <td>Dig Dug</td>
        <td><a href="https://github.com/opengateware/arcade-digdug/releases">0.1.0</a></td>
        <td>2022-09-20</td>
        <td>opengateware</td>
      </tr>
      <tr>
        <td><a href="https://github.com/Mazamars312/Analogue_Pocket_Neogeo">Analogue_Pocket_Neogeo</a></td>
        <td>Neo Geo</td>
        <td><a href="https://github.com/Mazamars312/Analogue_Pocket_Neogeo/releases">Alpha_0.7.5_2022-19-09</a></td>
        <td>2022-09-18</td>
        <td>Mazamars312</td>
      </tr>
      <tr>
        <td><a href="https://github.com/ericlewis/openfpga-dominos">openfpga-dominos</a></td>
        <td>Dominos</td>
        <td><a href="https://github.com/ericlewis/openfpga-dominos/releases">0.0.1</a></td>
        <td>2022-09-17</td>
        <td>ericlewis</td>
      </tr>
      <tr>
        <td><a href="https://github.com/opengateware/arcade-galaga">arcade-galaga</a></td>
        <td>Galaga</td>
        <td><a href="https://github.com/opengateware/arcade-galaga/releases">v0.1.0</a></td>
        <td>2022-09-15</td>
        <td>opengateware</td>
      </tr>
      <tr>
        <td><a href="https://github.com/ericlewis/openfpga-superbreakout">openfpga-superbreakout</a></td>
        <td>Super Breakout</td>
        <td><a href="https://github.com/ericlewis/openfpga-superbreakout/releases">0.0.1</a></td>
        <td>2022-09-14</td>
        <td>ericlewis</td>
      </tr>
      <tr>
        <td><a href="https://github.com/ericlewis/openfpga-lunarlander">openfpga-lunarlander</a></td>
        <td>Lunar Lander</td>
        <td><a href="https://github.com/ericlewis/openfpga-lunarlander/releases">0.9.1</a></td>
        <td>2022-09-13</td>
        <td>ericlewis</td>
      </tr>
      <tr>
        <td><a href="https://github.com/nullobject/openfpga-tecmo">openfpga-tecmo</a></td>
        <td>Tecmo</td>
        <td><a href="https://github.com/nullobject/openfpga-tecmo/releases">v2.0</a></td>
        <td>2022-09-10</td>
        <td>nullobject</td>
      </tr>
      <tr>
        <td><a href="https://github.com/agg23/openfpga-pong">openfpga-pong</a></td>
        <td>Pong</td>
        <td><a href="https://github.com/agg23/openfpga-pong/releases">1.2.0</a></td>
        <td>2022-09-09</td>
        <td>agg23</td>
      </tr>
      <tr>
        <td><a href="https://github.com/agg23/openfpga-arduboy">openfpga-arduboy</a></td>
        <td>Arduboy</td>
        <td><a href="https://github.com/agg23/openfpga-arduboy/releases">0.9.0</a></td>
        <td>2022-09-03</td>
        <td>agg23</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-SG1000">openFPGA-SG1000</a></td>
        <td>SG-1000</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-SG1000/releases">v1.2.0</a></td>
        <td>2022-08-27</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-SMS">openFPGA-SMS</a></td>
        <td>Master System</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-SMS/releases">v1.2.0</a></td>
        <td>2022-08-27</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-GG">openFPGA-GG</a></td>
        <td>Game Gear</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-GG/releases">v1.3.0</a></td>
        <td>2022-08-27</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-GBA">openFPGA-GBA</a></td>
        <td>Game Boy Advance</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-GBA/releases">v1.2.0</a></td>
        <td>2022-08-27</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-GB-GBC">openFPGA-GB-GBC</a></td>
        <td>Game Boy</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-GB-GBC/releases">v1.3.0</a></td>
        <td>2022-08-27</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spiritualized1997/openFPGA-GB-GBC">openFPGA-GB-GBC</a></td>
        <td>Game Boy Color</td>
        <td><a href="https://github.com/spiritualized1997/openFPGA-GB-GBC/releases">v1.3.0</a></td>
        <td>2022-08-27</td>
        <td>spiritualized1997</td>
      </tr>
      <tr>
        <td><a href="https://github.com/spacemen3/PDP-1">PDP-1</a></td>
        <td>PDP-1: Spacewar!</td>
        <td><a href="https://github.com/spacemen3/PDP-1/releases">v1.1.0</a></td>
        <td>2022-08-23</td>
        <td>spacemen3</td>
      </tr>
  </tbody>
</table>

---

## Extra scripts

I've added some extra scripts for utility purposes.

### updateMyCores

This script is to be used after you install the latest versions of your cores onto your Analogue Pocket and serves as a quick way to update your `myCores.json` file. Note that if you use this script without updating the cores on your device, you will be a version behind on any cores you didn't update.

```
npm run updateMyCores
```

### deleteNewCores

This script will merely empty the `newCores` folder on any downloaded and extracted cores in there.

```
npm run deleteNewCores
```

### downloadAllCores

This script will delete the contents of `newCores/` then download every core in the masterlist.

```
npm run downloadAllCores
```

### checkForNewCores

This script will check [this page](https://joshcampbell191.github.io/openfpga-cores-inventory/analogue-pocket.html) for any new cores that aren't current in `data/masterRepoList.json` and log any new cores in bold, red text if the page has a new core listed that isn't yet a part of this tool. I plan on checking daily and updating this tool as often as I can with new core info and I will use this script to do so, but it is here if you want to check for yourself.

```
npm run checkForNewCores
```

### checkNewFirmware

You can use this if you just want to check for new firmware from the terminal. If there is new firmware available, you'll get a red colored message letting you know with a direct download link to it. If you are on the latest version of the firmware, you'll see a green message in the terminal letting you know. I actually wired up the logic to download the firmware file, but I realized I didn't want to be responsible for someone bricking their Pocket in the event that the download was corrupted somehow and they installed it to their pocket. That said, you only get notified of an update and given a direct download link! ;)

```
npm run checkNewFirmware
```

### readMyCores

Plug in your Analogue Pocket's micro SD card, set `microSdPath` in the config to be the absolute path to your micro SD card, and run this script to generate a rough version of what you `myCores.json` file will look like. I say rough because I've noticed that the core developers are very inconsistent with versioning their cores. For example, if you use the Neo Geo core, the version number in the core.json file does not match the version number from GitHub. That said, you can use the `coresScraped.json` file as you base and get the accurate version number from `data/currentReleases.json` to quickly make this work as your myCores.json.

```
npm run readMyCores
```

### snapshotMicroSd

Plug in your Analogue Pocket's micro SD card, set `microSdPath` in the config to be the absolute path to your micro SD card, and run this script to generate a JSON file with information about your current Pocket set up. Data includes firmware version and file name (if one is present), your list of cores with data including your current version, and roms lists and counts for each core from your Assets directory. You can easily backup your micro SD card with a simple copy and paste. This is really to just provide you with some interesting data. If you were to lose or wipe your micro SD card and didn't have a backup available, the file created here could also serve as a guiide to help you get your micro SD back to the state you had it.

```
npm run snapshotMicroSd
```

### generateCoresTable

This just generates a markdown file of the cores from `extraScripts/getAllCoreInfo/allCoresInfo.json` that is sorted by the last updated date (most recent to least recent) and outputs the file to `extraScripts/generateTableForReadme/readmeCoresTable.md`. I just use this to generate the Cores table in the readme, but it is here and easily called if you want to run it for whatever reason.

```
npm run generateCoresTable
```
